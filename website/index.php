<?php
require 'config.php'; // Inclus le fichier config.php
spl_autoload_register(function($class)
{
	require 'app/' . $class . '.php'; // Chargement automatique des classes dans le dossier app
});
$app = new app(); // Instanciation de la casse app
$app->init(); // Execution de la fonction init()