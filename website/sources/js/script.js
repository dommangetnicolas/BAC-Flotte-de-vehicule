$(function() {
    /************************** Listeners ******************/

    /************************** Signin *********************/
    $('#submit-signin').click(function() {
        signin();
        return false;
    });

    /************************** Home *********************/

    if($('#recents-trips-home').val() != null) {
        loadRecentTripsHome();
        loadParcHome();
        loadMaintenanceToProvide();
    }

    /************************** Trips ********************/
    
    if($('#trips-trips').val() != null) {
        loadTrips();
    }

    $('.submit-delete-trips').click(function() {
        deleteTrips($(this).attr('id'));
    });

    /************************** Parc *********************/

    if($('#parc').val() != null) {
        loadParc();
    }

    $('.delete-vehicle-btn').click(function() {
        confirmDeleteParc($('.delete-vehicle-btn').attr('id'));
    })

    /********************** Maintenance *******************/

    if($('#maintenance').val() != null) {
        loadVehiclesMaintenance();
        loadMaintenance();
    }

    $('.modification-btn-modal').click(function() {
        modifyMaintenance($(this).attr('id'));
    });
});

/******************************************************************/
/****************************** SIGNIN ****************************/
/******************************************************************/

function signin() {
    var pseudoSignin = $('#pseudo-signin');
    var passSignin = $('#pass-signin');

    $.post('login/run', {
        pseudo:pseudoSignin.val(),
        pass:passSignin.val()
    }, function(data) {
        if(data == 1) {
            window.location = 'index';
        } else {
            pseudoSignin.addClass('is-invalid');
            passSignin.addClass('is-invalid');
        }
    });
}

/******************************************************************/
/****************************** HOME ******************************/
/******************************************************************/

function loadRecentTripsHome() {
    $('#recents-trips-home').load('trips/loadtrips/true');
}

function loadParcHome() {
    $('#parc-home').load('parc/loadParcHome');
}

function loadMaintenanceToProvide() {
    $('#maintenancetoprovide-home').load('maintenance/maintenanceToProvide');
}

/******************************************************************/
/****************************** TRIPS *****************************/
/******************************************************************/

function loadTrips() {
    if(document.location.hash.replace(/\#/g, '') != '') {
        $('#trips-trips').load('trips/loadtrips/', {
            id:document.location.hash.replace(/\#/g, '')
        });
        $('#title-trips').text('Trajets du scooter N°' + document.location.hash.replace(/\#/g, ''));
    } else {
        $('#trips-trips').load('trips/loadtrips/');
    }
}

function showDeleteModalTrips(id) {
    $('#delete-modal-trips').modal('show');
    $('.submit-delete-trips').attr('id', id);
}

function deleteTrips(id) {
    $.post('trips/deleteTrips', {
        id:id
    }, function(data) {
        loadTrips();
        $('#delete-modal-trips').modal('hide');
    });
}

/******************************************************************/
/****************************** Parc ******************************/
/******************************************************************/

function loadParc() {
    $('#parc').load('parc/loadParc');
}

function manageParc(nbr, state) {
    $.post("parc/manageParc", {
        id:nbr,
        available:state
    }, function(data, status) {
        if(data == 1) {
            loadParc();
        }
    });
}

function deleteVehicleParc(nbr) {
    $('.delete-vehicle-btn').attr('id', nbr);
    $('#delete-vehicle-parc-modal').modal('show');
}

function confirmDeleteParc(nbr) {
    $.post('parc/deleteVehicle', {
        id:nbr
    }, function(data, status) {
        if(data == 1) {
            alert("Le véhicule a été supprimé !");
            $('#delete-vehicle-parc-modal').modal('hide');
            loadParc();
        }
    })
}

/******************************************************************/
/************************** Maintenance****************************/
/******************************************************************/

function loadVehiclesMaintenance() {
    $('#vehicles-maintenance').load('maintenance/loadMaintenanceVehicles');
}

function loadMaintenance() {
    $('#maintenance').load('maintenance/loadMaintenance');
}

function showMaintenanceModal(nbr) {
    $('#maintenance-modal-title').text('Maintenance du scooter N°' + nbr);
    $('.modification-btn-modal').attr('id', nbr);
    $('#maintenance-modal').modal('show');
    $.post("maintenance/loadMaintenanceDistances",{
          id: nbr
        }, function(data,status){
            $('#totaldistance-modal').text('Distance total: ' + data['totaldistance'] + 'Km');
            $('#airfilter-input-modal').val(data['airfilter']);
            $('#forkoil-input-modal').val(data['forkoil']);
            $('#emptying-input-modal').val(data['emptying']);
            $('#brakes-input-modal').val(data['brakes']);
            $('#drivebelt-input-modal').val(data['drivebelt']);
            if(data['maintenance'] == 1) {
                $('#maintenance-checkbox-modal').prop('checked', true);
            } else {
                $('#maintenance-checkbox-modal').prop('checked', false);
            }
    }, "json");
}

function modifyMaintenance(nbr) {
    $.post("maintenance/modifyMaintenance", {
        id:nbr,
        airfilter:$('#airfilter-input-modal').val(),
        forkoil:$('#forkoil-input-modal').val(),
        emptying:$('#emptying-input-modal').val(),
        brakes:$('#brakes-input-modal').val(),
        drivebelt:$('#drivebelt-input-modal').val(),
        maintenance:$('#maintenance-checkbox-modal').prop('checked')
    }, function(data, status) {
        if(data == '1') {
            $('#maintenance-modal').modal('hide');
            loadMaintenance();
            loadVehiclesMaintenance();
        }
    });
}