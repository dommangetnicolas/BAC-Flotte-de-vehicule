<?php 

class loginModel extends model {
    private $_pseudo;
    private $_pass;

    public function init() {
        $this->_pseudo = $_POST['pseudo'];
        $this->_pass = hash('sha256', $_POST['pass']);
    
        $this->_req();
    }

    private function _req() {
        $sth = $this->db->prepare('SELECT * FROM admin WHERE pseudo = :pseudo AND pass = :pass');
        $sth->execute(array(
            'pseudo' => $this->_pseudo,
            'pass' => $this->_pass
        ));

        $count = $sth->rowCount();

        if($count == 1) {
            $data = $sth->fetch();
            session::init($data['id']);
            echo 1;
        } else {
            echo 0;
        }
    }
}