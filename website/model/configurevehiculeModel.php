<?php

class configurevehiculeModel extends model {
	private $_dbHost;
	private $_dbName;
	private $_dbUser;
	private $_dbPass;
    private $_id;
	private $_circumference;
	private $_stoppingDelay;
    private $_distance;
    private $_airFilter;
    private $_forkOil;
    private $_emptying;
    private $_brakes;
	private $_driveBelt;

	public function configure() {
		if(!empty($_POST['dbhost']) && !empty($_POST['dbname']) && !empty($_POST['dbuser']) && !empty($_POST['dbpass']) && !empty($_POST['id']) && !empty($_POST['circumference']) && !empty($_POST['stoppingdelay']) && !empty($_POST['distance']) && !empty($_POST['airfilter']) && !empty($_POST['forkoil']) && !empty($_POST['emptying']) && !empty($_POST['brakes']) && !empty($_POST['drivebelt'])) {
			$this->_dbHost = $_POST['dbhost'];
			$this->_dbName = $_POST['dbname'];
			$this->_dbUser = $_POST['dbuser'];
            $this->_dbPass = $_POST['dbpass'];
			$this->_id = $_POST['id'];
			$this->_circumference = $_POST['circumference'];
			$this->_stoppingDelay = $_POST['stoppingdelay'];
			$this->_distance = $_POST['distance'];
            $this->_airFilter = $_POST['airfilter'];
            $this->_forkOil = $_POST['forkoil'];
            $this->_emptying = $_POST['emptying'];
            $this->_brakes = $_POST['brakes'];
            $this->_driveBelt = $_POST['drivebelt'];
		} else {
			echo 'Veuillez remplir tous les champs ! <br>';
			echo '<a href="'. URL .'configurevehicule"><button>Retour</button></a>';
			return false;
		}
		
        if($this->_checkScooterId()) {
		    $this->_generateFiles();
        } else {
            echo 'Ce numéro de scooter est déjà utilisé ! <br>';
            echo '<a href="'. URL .'configurevehicule"><button>Retour</button></a>';
            return false;
        }
	}

	private function _generateFiles() {
		if (!file_exists('generatedFiles')) {
    		mkdir('generatedFiles', 0777, true);
		}

		if($this->_generateDbFile() && $this->_generateRegisterTrip() && $this->_generateScript() && $this->_generateSpeedometer()) {
			$this->_downloadFiles();
            $this->_addVehicleInDb();
		}
	}

	private function _generateDbFile() {
		$text = "import MySQLdb

class DB():

    host = '". $this->_dbHost ."'
    user = '". $this->_dbUser ."'
    password = '". $this->_dbPass ."'
    db = '". $this->_dbName ."'
    useDb = True

    def __init__(self, useDb = True):
        self.useDb = useDb

        if(self.useDb == True):
            self.connection = MySQLdb.connect(self.host, self.user, self.password, self.db)
            self.cursor = self.connection.cursor()

    def insert(self, query):
        try:
            self.cursor.execute(query)
            self.connection.commit()
        except:
            self.connection.rollback()

    def insertSqlFile(self, fileName):
        for line in open(fileName):
            self.cursor.execute(line)
            self.connection.commit()

    def createSqlFile(self, idDriver, idScooter, distance, duration, averageSpeed, fileName):
        file = open(fileName, \"a\")
        file.write(\"INSERT INTO trips (iddriver, idscooter, distance, duration, averagespeed) VALUES (\" + str(idDriver) + \", \" + str(idScooter) + \", \" + str(distance) + \", \" + str(duration) + \", \" + str(averageSpeed) + \")\\n\")
        file.close()

    def query(self, query):
        cursor = self.connection.cursor(MySQLdb.cursors.DictCursor)
        cursor.execute(query)

        return cursor.fetchall()

    def __del__(self):
        if(self.useDb == True):
            self.connection.close()";
		$handle = fopen("generatedFiles/database.py", "w");
		fwrite($handle, $text);
		fclose($handle);

		return true;
	}

	private function _generateRegisterTrip() {
		$text = "import urllib2
import database
import os

class RegisterTrip:
    idDriver = 0
    idScooter = 0
    distance = 0
    duration = 0
    averageSpeed = 0
    fileName = 'trips.sql'

    def checkConnection(self):
        try:
            urllib2.urlopen(\"http://192.168.43.22\").close()
            
        except urllib2.URLError:
            return False

        else:
            return True

    def setValues(self, idDriver, idScooter, distance, duration, averageSpeed):
        self.idDriver = idDriver
        self.idScooter = idScooter
        self.distance = distance
        self.duration = duration
        self.averageSpeed = averageSpeed

    def addTrip(self):
        db = database.DB()

        if os.path.exists(self.fileName) and self.checkConnection() == True:
            self.uploadSqlFile()
            
        db.insert(\"INSERT INTO trips (iddriver, idscooter, distance, duration, averagespeed) VALUES ('%s', '%s', '%s', '%s', '%s')\" % (self.idDriver, self.idScooter, self.distance, self.duration, self.averageSpeed))

    def createSqlFile(self):
        db = database.DB(False)
        db.createSqlFile(self.idDriver, self.idScooter, self.distance, self.duration, self.averageSpeed, self.fileName)

    def uploadSqlFile(self):
        if os.path.exists(self.fileName) and self.checkConnection() == True:
                db = database.DB()
                db.insertSqlFile(self.fileName)
                os.remove(self.fileName)";

		$handle = fopen("generatedFiles/registertrip.py", "w");
		fwrite($handle, $text);
		fclose($handle);

		return true;
	}

	private function _generateScript() {
		$text = "import speedometer
import RPi.GPIO as GPIO

def endprogram():
        GPIO.cleanup()

if __name__ == '__main__':
        speedometer = speedometer.Speedometer()
        try:
                speedometer.loop()
        except KeyboardInterrupt:
                endprogram()";

		$handle = fopen("generatedFiles/script.py", "w");
		fwrite($handle, $text);
		fclose($handle);

		return true;
	}

	private function _generateSpeedometer() {
		$text = "#!/usr/bin/env python
import RPi.GPIO as GPIO
import time
import registertrip
import os

class Speedometer:
        register = registertrip.RegisterTrip()
        uploadSqlInterval = time.time()
        reedPin = 11
        idDriver = 4
        idScooter = 8
        circumference = ". $this->_circumference ."
        stoppingDelay = ". $this->_stoppingDelay ."

        def __init__(self):
                GPIO.setmode(GPIO.BOARD)
                GPIO.setup(self.reedPin, GPIO.IN)
        
        def loop(self):
                while True:
                    loop = 0
                    start = 0
                    duration = 0
                    distance = 0
                    stopping = 0
                    count = 0
                    stopped = False

                    if GPIO.input(self.reedPin) == 0:
                        start = time.time()
                        while loop == 0:
                            if GPIO.input(self.reedPin) == 0:
                                while(GPIO.input(self.reedPin) == 0):
                                    if(count == 0):
                                        stopping = 0
                                        stopped = False
                                        distance +=  self.circumference
                                        print(str(distance) + \" Metres\")
                                        count += 1
                                        time.sleep(0.05)
                                    else:
                                        pass
                            elif stopped == False and GPIO.input(self.reedPin) == 1:
                                count = 0
                                stopping = time.time()
                                stopped = True
                                time.sleep(0.001)
                            elif stopped == True and GPIO.input(self.reedPin) == 1 and time.time() - stopping < self.stoppingDelay:
                                count = 0
                                time.sleep(0.001)
                            else:
                                count = 0
                                distance /= 1000
                                duration = (time.time() - start - self.stoppingDelay / 3600) / 3600
                                self.endOfTrip(distance, duration, self.computeAverageSpeed(distance, duration))
                                loop = 1

                    if(time.time() - self.uploadSqlInterval > 30):
                        self.uploadSqlInterval = time.time()
                        self.register.uploadSqlFile()

                    time.sleep(0.001)

        def computeAverageSpeed(self, distance, duration):
            averageSpeed = (distance / duration)
            return averageSpeed

        def endOfTrip(self, distance, duration, averageSpeed):
            print(\"Distance parcourue: \" + str(distance))
            print(\"Temps de parcours: \" + str(duration))
            print(\"Vitesse moyenne: \" + str(averageSpeed))
            if self.register.checkConnection() == True:
                self.register.setValues(self.idDriver, self.idScooter, distance, duration, averageSpeed)
                self.register.addTrip()
                self.register.uploadSqlFile()
            else:
                self.register.setValues(self.idDriver, self.idScooter, distance, duration, averageSpeed)
                self.register.createSqlFile()";

		$handle = fopen("generatedFiles/speedometer.py", "w");
		fwrite($handle, $text);
		fclose($handle);

		return true;
	}

	private function _downloadFiles() {
		$files = array('generatedFiles/database.py', 'generatedFiles/registertrip.py', 'generatedFiles/script.py', 'generatedFiles/speedometer.py');
		$zipName = 'scooter'. $this->_id .'.zip';
		$zipPath = 'generatedFiles/fleet.zip';
		$zip = new ZipArchive;
		$zip->open($zipPath, ZipArchive::CREATE);
		foreach ($files as $file) {
		  $zip->addFile($file);
		}
		$zip->close();

		header('Content-Type: application/zip');
		header('Content-disposition: attachment; filename='.$zipName);
		header('Content-Length: ' . filesize($zipPath));
		readfile($zipPath);
	}

    private function _addVehicleInDb() {
        $sth = $this->db->prepare('INSERT INTO scooter (id, airfilter, forkoil, emptying, brakes, drivebelt) VALUES (:id, :airfilter, :forkoil, :emptying, :brakes, :drivebelt)');
        $sth->execute(array(
            'airfilter' => $this->_airFilter,
            'forkoil' => $this->_forkOil,
            'emptying' => $this->_emptying,
            'brakes' => $this->_brakes,
            'drivebelt' => $this->_driveBelt,
            'id' => $this->_id
        ));

        $sth = $this->db->prepare('INSERT INTO trips (iddriver, idscooter, distance) VALUES (0, :idscooter, :distance)');
        $sth->execute(array(
            'idscooter' => $this->_id,
            'distance' => $this->_distance
        ));
    }

    private function _checkScooterId() {
        $sth = $this->db->prepare('SELECT * FROM scooter WHERE id = :id');
        $sth->execute(array(
            'id' => $this->_id
        ));

        if($sth->rowCount() == 0) {
            return true;
        } else {
            return false;
        }
    }
}