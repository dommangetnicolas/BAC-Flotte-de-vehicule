<?php

class tripsModel extends model {
    private $_id;

    public function loadTrips($recent) {
        if($recent == true) {
            $sth = $this->db->prepare('SELECT * FROM trips ORDER BY tripdate DESC LIMIT 5');
            $sth->execute();

            while($data = $sth->fetch()) {
                $sth1 = $this->db->prepare('SELECT * FROM drivers WHERE id = :id');
                $sth1->execute(array(
                    'id' => $data['iddriver']
                ));

                $data1 = $sth1->fetch();
                $data['firstname'] = $data1['firstname'];
                $data['lastname'] = $data1['lastname'];

                echo '<li class="list-group-item d-flex justify-content-between align-items-center"><span><strong>Scooter N°'. $data['idscooter'] .'</strong> '. $data['firstname'] .' '. $data['lastname'] .'</span> '. round($data['distance'], 2) .'Km '. round($data['duration'] * 60, 2) .'min '. round($data['averagespeed'], 1) .'Km/h</li>';
            }

        } elseif(isset($_POST['id'])) {
            $sth = $this->db->prepare('SELECT * FROM drivers, trips WHERE idscooter = :id');
            $sth->execute(array(
                'id' => $_POST['id']
            ));

            if($sth->rowCount() == 0) {
                echo('Il n\'y a pas de trajets pour ce véhicule');
            } else {
                $i = 0;
                while($data = $sth->fetch()) {
                    if($i % 2 == 0) {
                        echo '<li class="list-group-item d-flex justify-content-between align-items-center bg-white"><span><strong>Scooter N°'. $data['idscooter'] .'</strong> '. $data['firstname'] .' '. $data['lastname'] .'</span><span>'. round($data['distance'], 2) .'Km '. round($data['duration'] * 60, 2) .'min '. round($data['averagespeed'], 1) .'Km/h <i class="fas fa-trash text-danger delete-btn-trips" onclick="showDeleteModalTrips('. $data['id'] .')"></i></span></li>';
                        $i++;
                    } else {
                        echo '<li class="list-group-item d-flex justify-content-between align-items-center bg-light"><span><strong>Scooter N°'. $data['idscooter'] .'</strong> '. $data['firstname'] .' '. $data['lastname'] .'</span><span>'. round($data['distance'], 2) .'Km '. round($data['duration'] * 60, 2) .'min '. round($data['averagespeed'], 1) .'Km/h <i class="fas fa-trash text-danger delete-btn-trips" onclick="showDeleteModalTrips('. $data['id'] .')"></i></span></li>';
                        $i--;
                    }
                }
            }
        } else {
            $sth = $this->db->prepare('SELECT * FROM trips ORDER BY tripdate DESC');
            $sth->execute();

            $i = 0;
            while($data = $sth->fetch()) {
                $sth1 = $this->db->prepare('SELECT * FROM drivers WHERE id = :id');
                $sth1->execute(array(
                    'id' => $data['iddriver']
                ));

                $data1 = $sth1->fetch();
                $data['firstname'] = $data1['firstname'];
                $data['lastname'] = $data1['lastname'];

                if($data['iddriver'] == 0) {
                    echo '<li class="list-group-item d-flex justify-content-between align-items-center bg-info"><span><strong>Scooter N°'. $data['idscooter'] .' Distance d\'initialisation: '. $data['distance'] .'Km</strong></li>';
                } else {
                    if($i % 2 == 0) {
                        echo '<li class="list-group-item d-flex justify-content-between align-items-center bg-white"><span><strong>Scooter N°'. $data['idscooter'] .'</strong> '. $data['firstname'] .' '. $data['lastname'] .'</span><span>'. round($data['distance'], 2) .'Km '. round($data['duration'] * 60, 2) .'min '. round($data['averagespeed'], 1) .'Km/h <i class="fas fa-trash text-danger delete-btn-trips" onclick="showDeleteModalTrips('. $data['id'] .')"></i></span></li>';
                        $i++;
                    } else {
                        echo '<li class="list-group-item d-flex justify-content-between align-items-center bg-light"><span><strong>Scooter N°'. $data['idscooter'] .'</strong> '. $data['firstname'] .' '. $data['lastname'] .'</span><span>'. round($data['distance'], 2) .'Km '. round($data['duration'] * 60, 2) .'min '. round($data['averagespeed'], 1) .'Km/h <i class="fas fa-trash text-danger delete-btn-trips" onclick="showDeleteModalTrips('. $data['id'] .')"></i></span></li>';
                        $i--;
                    }
                }
            }
        }
    }

    public function deleteTrips() {
        $this->_id = $_POST['id'];

        $this->_deleteTripsReq();
    }

    private function _deleteTripsReq() {
        $sth = $this->db->prepare('DELETE FROM trips WHERE id = :id');
        $sth->execute(array(
            'id' => $this->_id
        ));
    }
}