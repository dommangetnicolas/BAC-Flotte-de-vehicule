<?php

class parcModel extends model {

    public function loadParc() {
        $sth = $this->db->prepare('SELECT * FROM scooter');
        $sth->execute();

        while($data = $sth->fetch()) {
            $sth1 = $this->db->prepare('SELECT SUM(distance) FROM trips WHERE idscooter = :id');
            $sth1->execute(array(
                'id' => $data['id']
            ));

            $totalDistance = $sth1->fetch();
            $data['distance'] = $totalDistance['SUM(distance)'];

            if($data['available'] == 1) {
                echo '<li class="list-group-item d-flex justify-content-between align-items-center text-white bg-success"><span>Scooter N°<strong>'. $data['id'] .'</strong> Distance totale: <strong>'. round($data['distance'], 3) .'</strong>Km</span><span><button type="button" class="btn btn-danger" onclick="manageParc('. $data['id'] .', 0)">Marquer indisponible</button> <a href="'. URL .'trips#'. $data['id'] .'"><i class="fas fa-road text-white"></i></a> <a href="'. URL .'maintenance"><i class="fas fa-wrench text-white"></i></a> <i class="far fa-trash-alt text-white" onclick="deleteVehicleParc('. $data['id'] .')"></i></span></li>';
            } elseif($data['available'] == 0 && $data['maintenance'] == 1) {
                echo '<li class="list-group-item d-flex justify-content-between align-items-center text-white bg-warning"><span>Scooter N°<strong>'. $data['id'] .'</strong> Distance totale: <strong>'. round($data['distance'], 3) .'</strong>Km</span><span><a href="'. URL .'trips#'. $data['id'] .'"><i class="fas fa-road text-white"></i></a> <a href="'. URL .'maintenance"><i class="fas fa-wrench text-white"></i></a> <i class="far fa-trash-alt text-white" onclick="deleteVehicleParc('. $data['id'] .')"></i></span></li>';
            } elseif($data['available'] == 0) {
                echo '<li class="list-group-item d-flex justify-content-between align-items-center text-white bg-danger"><span>Scooter N°<strong>'. $data['id'] .'</strong> Distance totale: <strong>'. round($data['distance'], 3) .'</strong>Km</span><span><button type="button" class="btn btn-success" onclick="manageParc('. $data['id'] .', 1)"">Marquer disponible</button> <a href="'. URL .'trips#'. $data['id'] .'"><i class="fas fa-road text-white"></i></a> <a href="'. URL .'maintenance"><i class="fas fa-wrench text-white"></i></a> <i class="far fa-trash-alt text-white" onclick="deleteVehicleParc('. $data['id'] .')"></i></span></li>';
            }
        }
    }

    public function loadParcHome() {
            $sth = $this->db->prepare('SELECT * FROM scooter');
            $sth->execute();

        while($data = $sth->fetch()) {
            if($data['available'] == 1) {
                echo '<div class="col-3 rounded-div bg-success">Scooter N°'. $data['id'] .'</div>';
            } elseif($data['available'] == 0 && $data['maintenance'] == 1) {
                echo '<div class="col-3 rounded-div bg-warning">Scooter N°'. $data['id'] .'</div>';
            } elseif($data['available'] == 0) {
                echo '<div class="col-3 rounded-div bg-danger">Scooter N°'. $data['id'] .'</div>';
            }
        }
    }

    public function manageParc() {
        $sth = $this->db->prepare('UPDATE scooter SET available = :available WHERE id = :id');
        $sth->execute(array(
            'id' => $_POST['id'],
            'available' => $_POST['available']
        ));

        if($sth) {
            echo true;
        }
    }

    public function deleteVehicle() {
        $sth = $this->db->prepare('DELETE FROM scooter WHERE id = :id');
        $sth->execute(array(
            'id' => $_POST['id']
        ));

        if($sth) {
            $sth = $this->db->prepare('DELETE FROM trips WHERE idscooter = :id');
            $sth->execute(array(
                'id' => $_POST['id']
            ));

            if($sth) {
                echo true;
            }
        }
    }
}