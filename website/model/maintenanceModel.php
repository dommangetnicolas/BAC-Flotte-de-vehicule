<?php

class maintenanceModel extends model {
    private $_airFilterDistance = 6000;
    private $_forkOilDistance = 10000;
    private $_emptyingDistance = 6000;
    private $_brakesDistance = 5000;
    private $_driveBeltDistance = 15000;

	public function loadMaintenanceVehicles() {
		$sth = $this->db->prepare('SELECT * FROM scooter');
		$sth->execute();

		while($data = $sth->fetch()) {
            $sth1 = $this->db->prepare('SELECT SUM(distance) FROM trips WHERE idscooter = :id');
            $sth1->execute(array(
                'id' => $data['id']
            ));

            $totalDistance = $sth1->fetch();
            $data['distance'] = round($totalDistance['SUM(distance)'], 1);

            if(($this->_airFilterDistance - ($data['distance'] - $data['airfilter'])) < 0) {
                $provide = 1;
            } elseif(($this->_forkOilDistance - ($data['distance'] - $data['forkoil'])) < 0) {
                $provide = 1;
            } elseif(($this->_emptyingDistance - ($data['distance'] - $data['emptying'])) < 0) {
                $provide = 1;
            } elseif(($this->_brakesDistance - ($data['distance'] - $data['brakes'])) < 0) {
                $provide = 1;
            } elseif(($this->_driveBeltDistance - ($data['distance'] - $data['drivebelt'])) < 0) {
                $provide = 1;
            } else {
                $provide = 0;
            }



			if($data['maintenance'] == 1) {
				echo '<div class="col-3 rounded-div bg-danger vehicle-maintenance" id="'. $data['id'] .'" onclick="showMaintenanceModal('. $data['id'] .')">Scooter N°'. $data['id'] .'</div>';
            } elseif($provide == 1) {
                echo '<div class="col-3 rounded-div bg-warning vehicle-maintenance" id="'. $data['id'] .'" onclick="showMaintenanceModal('. $data['id'] .')">Scooter N°'. $data['id'] .'</div>';
			} else {
				echo '<div class="col-3 rounded-div bg-success vehicle-maintenance" id="'. $data['id'] .'" onclick="showMaintenanceModal('. $data['id'] .')">Scooter N°'. $data['id'] .'</div>';
			}
		}
 	}

 	public function loadMaintenance() {
 		$sth = $this->db->prepare('SELECT * FROM scooter');
 		$sth->execute();

 		while($data = $sth->fetch()) {
 			$sth1 = $this->db->prepare('SELECT SUM(distance) FROM trips WHERE idscooter = :id');
            $sth1->execute(array(
                'id' => $data['id']
            ));

            $totalDistance = $sth1->fetch();
            $data['distance'] = round($totalDistance['SUM(distance)'], 1);

 			echo '<tr>
                        <th scope="row">N°'. $data['id'] .'</th>
                        <td>'. $data['distance'] .'Km</td>';

                        if(($this->_airFilterDistance - ($data['distance'] - $data['airfilter'])) < 1000 && ($this->_airFilterDistance - ($data['distance'] - $data['airfilter'])) >= 0) {
                            echo '<td class="text-warning">'. ($this->_airFilterDistance - ($data['distance'] - $data['airfilter'])) .'</td>';
                        } elseif(($this->_airFilterDistance - ($data['distance'] - $data['airfilter'])) < 0) {
                            echo '<td class="text-danger">'. ($this->_airFilterDistance - ($data['distance'] - $data['airfilter'])) .'</td>';
                        } else {
                            echo '<td class="text-success">'. ($this->_airFilterDistance - ($data['distance'] - $data['airfilter'])) .'</td>';
                        }

                        if(($this->_forkOilDistance - ($data['distance'] - $data['forkoil'])) < 1000 && ($this->_forkOilDistance - ($data['distance'] - $data['forkoil'])) >= 0) {
                            echo '<td class="text-warning">'. ($this->_forkOilDistance - ($data['distance'] - $data['forkoil'])) .'</td>';
                        } elseif(($this->_forkOilDistance - ($data['distance'] - $data['forkoil'])) < 0) {
                            echo '<td class="text-danger">'. ($this->_forkOilDistance - ($data['distance'] - $data['forkoil'])) .'</td>';
                        } else {
                            echo '<td class="text-success">'. ($this->_forkOilDistance - ($data['distance'] - $data['forkoil'])) .'</td>';
                        }

                        if(($this->_emptyingDistance - ($data['distance'] - $data['emptying'])) < 1000 && ($this->_emptyingDistance - ($data['distance'] - $data['emptying'])) >= 0) {
                            echo '<td class="text-warning">'. ($this->_emptyingDistance - ($data['distance'] - $data['emptying'])) .'</td>';
                        } elseif(($this->_emptyingDistance - ($data['distance'] - $data['emptying'])) < 0) {
                            echo '<td class="text-danger">'. ($this->_emptyingDistance - ($data['distance'] - $data['emptying'])) .'</td>';
                        } else {
                            echo '<td class="text-success">'. ($this->_emptyingDistance - ($data['distance'] - $data['emptying'])) .'</td>';
                        }

                        if(($this->_brakesDistance - ($data['distance'] - $data['brakes'])) < 1000 && ($this->_brakesDistance - ($data['distance'] - $data['brakes'])) >= 0) {
                            echo '<td class="text-warning">'. ($this->_brakesDistance - ($data['distance'] - $data['brakes'])) .'</td>';
                        } elseif(($this->_brakesDistance - ($data['distance'] - $data['brakes'])) < 0) {
                            echo '<td class="text-danger">'. ($this->_brakesDistance - ($data['distance'] - $data['brakes'])) .'</td>';
                        } else {
                            echo '<td class="text-success">'. ($this->_brakesDistance - ($data['distance'] - $data['brakes'])) .'</td>';
                        }

                        if(($this->_driveBeltDistance - ($data['distance'] - $data['drivebelt'])) < 1000 && ($this->_driveBeltDistance - ($data['distance'] - $data['drivebelt'])) >= 0) {
                            echo '<td class="text-warning">'. ($this->_driveBeltDistance - ($data['distance'] - $data['drivebelt'])) .'</td>';
                        } elseif(($this->_driveBeltDistance - ($data['distance'] - $data['drivebelt'])) < 0) {
                            echo '<td class="text-danger">'. ($this->_driveBeltDistance - ($data['distance'] - $data['drivebelt'])) .'</td>';
                        } else {
                            echo '<td class="text-success">'. ($this->_driveBeltDistance - ($data['distance'] - $data['drivebelt'])) .'</td>';
                        }

                    echo '</tr>';
 		}
 	}

    public function loadMaintenanceDistances() {
        $id = $_POST['id'];

        $sth = $this->db->prepare('SELECT * FROM scooter WHERE id = :id');
        $sth->execute(array(
            'id' => $id
        ));

        $sth1 = $this->db->prepare('SELECT SUM(distance) FROM trips WHERE idscooter = :id');
        $sth1->execute(array(
            'id' => $id
        ));

        $data = $sth->fetch();
        $data1 = $sth1->fetch();

        $data['totaldistance'] = $data1['SUM(distance)'];

        echo json_encode($data);
    }

    public function modifyMaintenance() {
        $id = $_POST['id'];
        if($_POST['maintenance'] == 'true') {
            $maintenance = 1;
            $available = 0;
        } else {
            $maintenance = 0;
            $available = 1;
        }
        $airfilter = $_POST['airfilter'];
        $forkoil = $_POST['forkoil'];
        $emptying = $_POST['emptying'];
        $brakes = $_POST['brakes'];
        $drivebelt = $_POST['drivebelt'];

        $sth = $this->db->prepare('UPDATE scooter SET available = :available, maintenance = :maintenance, airfilter = :airfilter, forkoil = :forkoil, emptying = :emptying, brakes =:brakes, drivebelt = :drivebelt WHERE id = :id');
        $sth->execute(array(
            'available' => $available,
            'maintenance' => $maintenance,
            'airfilter' => $airfilter,
            'forkoil' => $forkoil,
            'emptying' => $emptying,
            'brakes' => $brakes,
            'drivebelt' => $drivebelt,
            'id' => $id
        ));

        if($sth) {
            echo true;
        }
    }

    public function maintenanceToProvide() {
        $sth = $this->db->prepare('SELECT * FROM scooter');
        $sth->execute();

        $maintenance = 0;

        while($data = $sth->fetch()) {
            $sth1 = $this->db->prepare('SELECT SUM(distance) FROM trips WHERE idscooter = :id');
            $sth1->execute(array(
                'id' => $data['id']
            ));

            $totalDistance = $sth1->fetch();
            $data['distance'] = round($totalDistance['SUM(distance)'], 1);

            if(($this->_airFilterDistance - ($data['distance'] - $data['airfilter'])) < 1000) {
                echo '<li class="list-group-item d-flex justify-content-between align-items-center"><span><strong>Scooter N°'. $data['id'] .'</strong></span> Filtre à air</li>';
                $maintenance++;
            }

            if(($this->_forkOilDistance - ($data['distance'] - $data['forkoil'])) < 1000) {
                echo '<li class="list-group-item d-flex justify-content-between align-items-center"><span><strong>Scooter N°'. $data['id'] .'</strong></span> Huile fourche</li>';
                $maintenance++;
            }

            if(($this->_emptyingDistance - ($data['distance'] - $data['emptying'])) < 1000) {
                echo '<li class="list-group-item d-flex justify-content-between align-items-center"><span><strong>Scooter N°'. $data['id'] .'</strong></span> Vidange</li>';
                $maintenance++;
            }

            if(($this->_brakesDistance - ($data['distance'] - $data['brakes'])) < 1000) {
                echo '<li class="list-group-item d-flex justify-content-between align-items-center"><span><strong>Scooter N°'. $data['id'] .'</strong></span> Freins</li>';
                $maintenance++;
            }

            if(($this->_driveBeltDistance - ($data['distance'] - $data['drivebelt'])) < 1000) {
                echo '<li class="list-group-item d-flex justify-content-between align-items-center"><span><strong>Scooter N°'. $data['id'] .'</strong></span> Courroie de transmission</li>';
                $maintenance++;
            }
        }

        if($maintenance == 0) {
            echo '<li class="list-group-item d-flex justify-content-between align-items-center"><span><strong>Aucune maintenance à prévoir !</strong></span></li>';
        }
    }
}
