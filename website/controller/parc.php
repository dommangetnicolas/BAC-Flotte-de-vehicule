<?php

class parc extends controller {
    public function default() {
        $this->view->render(array('folder' => 'parc', 'title' => 'Parc'));
    }

    public function loadParc() {
        $this->model->loadParc();
    }

    public function loadParcHome() {
        $this->model->loadParcHome();
    }

    public function manageParc() {
    	$this->model->manageParc();
    }

    public function deleteVehicle() {
        $this->model->deleteVehicle();
    }
}