<?php

class logout extends controller {

    public function default() {
        session::stop();
    }
}