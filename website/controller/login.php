<?php

class login extends controller {

    public function __construct()
	{
		parent::__construct('connected');
	}

    public function default() {
        $this->view->render(array('folder' => 'login', 'navbar' => 'none'));
    }

    public function run() {
        $this->model->init();
    }
}