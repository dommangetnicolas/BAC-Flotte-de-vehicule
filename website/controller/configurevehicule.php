<?php

class configurevehicule extends controller {
	public function default() {
		$this->view->render(array('folder' => 'configurevehicule', 'title' => 'configuration'));
	}

	public function configure() {
		$this->model->configure();
	}
}