<?php

class trips extends controller {
    public function default() {
        $this->view->render(array('folder' => 'trips', 'title' => 'Tous les trajets'));
    }

    public function loadTrips($recent = false) {
        $this->model->loadTrips($recent);
    }

    public function deleteTrips() {
        $this->model->deleteTrips();
    }
}