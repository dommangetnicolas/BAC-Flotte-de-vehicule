<?php

class maintenance extends controller {
    public function default() {
        $this->view->render(array('folder' => 'maintenance', 'title' => 'maintenance'));
    }

    public function loadMaintenanceVehicles() {
    	$this->model->loadMaintenanceVehicles();
    }

    public function loadMaintenance() {
    	$this->model->loadMaintenance();
    }

    public function loadMaintenanceDistances() {
    	$this->model->loadMaintenanceDistances();
    }

    public function modifyMaintenance() {
        $this->model->modifyMaintenance();
    }

    public function maintenanceToProvide() {
        $this->model->maintenanceToProvide();
    }
}