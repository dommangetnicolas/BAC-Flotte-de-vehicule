<div class="container">
    <div class="row justify-content-center">
        <div class="rounded-div" style="margin-top: 100px">
            <form class="form-signin">
                <h1 class="h3 mb-3 font-weight-normal">Connectez vous !</h1>
                <label for="pseudo-signin" class="sr-only">Email address</label>
                <input type="text" id="pseudo-signin" class="form-control" placeholder="Identifiant" required autofocus>
                <label for="pass-signin" class="sr-only">Password</label>
                <input type="password" id="pass-signin" class="form-control" placeholder="Mot de passe" required>
                <div class="checkbox mb-3">
                    <label>
                        <input type="checkbox" value="remember-me"> Se souvenir de moi
                    </label>
                </div>
                
                <button class="btn btn-lg btn-primary btn-block" id="submit-signin" type="submit">Connexion</button>
            </form>
        </div>
    </div>
</div>