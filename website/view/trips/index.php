<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="rounded-div">
                <h6 id="title-trips">Tous les trajets</h6>
                <ul class="list-group" id="trips-trips">
                    
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="delete-modal-trips" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        Êtes vous sur de supprimer ce trajet ?
      </div>
      <div class="modal-footer">
        <a href="#" class="btn btn-outline-danger submit-delete-trips">Supprimer</a>
        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Annuler</button>
      </div>
    </div>
  </div>
</div>