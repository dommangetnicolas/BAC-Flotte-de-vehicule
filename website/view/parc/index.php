<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="rounded-div">
                <div class="col-12">
                    <h6>Parc</h6>
                        <div class="col-12 list-group" id="parc">
                        </div>
                </div><br>
                <div class="col-12">
                    <div class="row">
                        <h6><span class="badge badge-success">Disponible</span></h6>&nbsp
                        <h6><span class="badge badge-warning">En maintenance</span></h6>&nbsp
                        <h6><span class="badge badge-danger">Indisponible</span></h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="delete-vehicle-parc-modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Suppresion d'un véhicule</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Vous êtes sur le point de supprimer un véhicule de la flotte, ses trajets, ses données de maintenance seront ainsi effacés de la base de donnée.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary delete-vehicle-btn">Confirmer</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
      </div>
    </div>
  </div>
</div>