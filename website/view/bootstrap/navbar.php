<nav class="navbar navbar-expand-lg navbar-dark" style="background-color: #304154">
  <a class="navbar-brand" href="<?= URL ?>">Flotte de véhicules</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="<?= URL ?>">Accueil</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= URL ?>trips">Trajets</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= URL ?>parc">Parc</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= URL ?>maintenance">Maintenance</a>
      </li>
    </ul>
    <ul class="navbar-nav">
      <li class="nav-item">
      <a href="<?= URL ?>logout" class="btn btn-outline-danger">Déconnexion</a>
      </li>
    </ul>
  </div>
</nav>