<div class="container">
	<div class="row">
		<div class="col-12">
			<div class="rounded-div">
				<h6>Configuration d'un nouveau véhicule <a href="#" class="text-primary" data-toggle="modal" data-target="#help-configure"><i class="fas fa-question-circle"></i></a></h6><br>

				<form action="<?= URL ?>configurevehicule/configure" method="post">
					<h6>Connexion du véhicule à la base de données</h6>
					<div class="row">
						<div class="col">
							<input type="text" class="form-control" name="dbhost" placeholder="Adresse IP">
						</div>
						<div class="col">
							<input type="text" class="form-control" name="dbname" placeholder="Nom de la base">
						</div>
					</div><br>

					<div class="row">
						<div class="col">
							<input type="text" class="form-control" name="dbuser" placeholder="Identifiant">
						</div>
						<div class="col">
							<input type="password" class="form-control" name="dbpass" placeholder="Mot de passe">
						</div>
					</div><br>

					<h6>Configuration des données du véhicule</h6>

					<div class="row">
						<div class="col">
							<input type="text" class="form-control" name="circumference" placeholder="Circonférence de la roue">
						</div>
						<div class="col">
							<input type="number" class="form-control" name="stoppingdelay" placeholder="Délais d'arrêt">
						</div>
					</div><br>

					<div class="row">
						<div class="col">
							<input type="text" class="form-control" name="distance" placeholder="Distance actuelle">
						</div>
						<div class="col">
							<input type="text" class="form-control" name="id" placeholder="Numéro du véhicule">
						</div>
					</div><br>

					<h6>Veuillez entrer pour chaque élément le kilométrage du dernier entretien</h6>

					<div class="row">
						<div class="col">
							<input type="number" class="form-control" name="airfilter" placeholder="Filtre à air">
						</div>
						<div class="col">
							<input type="number" class="form-control" name="forkoil" placeholder="Huile fourche">
						</div>
					</div><br>

					<div class="row">
						<div class="col">
							<input type="number" class="form-control" name="emptying" placeholder="Vidange">
						</div>
						<div class="col">
							<input type="nummber" class="form-control" name="brakes" placeholder="Freins">
						</div>
					</div><br>

					<div class="row">
						<div class="col-6">
							<input type="number" class="form-control" name="drivebelt" placeholder="Courroie de transmission">
						</div>
					</div><br>

					<button type="submit" class="btn btn-primary">Enregistrer</button>
				</form>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="help-configure">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Aide à la configuration</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Pour configurer un nouveau véhicule, vous devez remplir les champs du formulaire. Une fois remplis correctement, les informations du scooter seront ajoutées à la base de donnée et les fichiers que vous devrais mettre en place sur le dispotif du véhicule seront automatiquement téléchargés.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>