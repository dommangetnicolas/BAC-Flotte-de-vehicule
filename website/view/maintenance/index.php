<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="rounded-div">
                <div class="col-12 row" id="vehicles-maintenance"></div>
                <div class="col-12">
                    <div class="row">
                        <h6><span class="badge badge-success">Rien à prévoir</span></h6>&nbsp
                        <h6><span class="badge badge-warning">Maintenance à prévoir</span></h6>&nbsp
                        <h6><span class="badge badge-danger">En maintenance</span></h6>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="rounded-div">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Scooter</th>
                            <th scope="col">Distance</th>
                            <th scope="col">Filtre à air</th>
                            <th scope="col">Huile fourche</th>
                            <th scope="col">Vidange</th>
                            <th scope="col">Freins</th>
                            <th scope="col">Courroie de transmission</th>
                        </tr>
                        <tr>
                            <th scope="col"></th>
                            <th scope="col"></th>
                            <th scope="col">(6000Km)</th>
                            <th scope="col">(10000Km)</th>
                            <th scope="col">(6000Km)</th>
                            <th scope="col">(5000Km)</th>
                            <th scope="col">(15000Km)</th>
                        </tr>
                    </thead>
                    <tbody id="maintenance">
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" tabindex="-1" role="dialog" id="maintenance-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="maintenance-modal-title">Maintenance</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h5 id="totaldistance-modal"></h5>
                <form>
                    <div class="form-group">
                        <label for="airfilter-input-modal" class="col-form-label">Filtre à air:</label>
                        <input type="number" class="form-control" id="airfilter-input-modal">
                    </div>

                    <div class="form-group">
                        <label for="forkoil-input-modal" class="col-form-label">Huile fourche:</label>
                        <input type="number" class="form-control" id="forkoil-input-modal">
                    </div>

                    <div class="form-group">
                        <label for="emptying-input-modal" class="col-form-label">Vidange:</label>
                        <input type="number" class="form-control" id="emptying-input-modal">
                    </div>

                    <div class="form-group">
                        <label for="brakes-input-modal" class="col-form-label">Freins:</label>
                        <input type="number" class="form-control" id="brakes-input-modal">
                    </div>

                    <div class="form-group">
                        <label for="drivebelt-input-modal" class="col-form-label">Courroie de transmission:</label>
                        <input type="number" class="form-control" id="drivebelt-input-modal">
                    </div>

                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" id="maintenance-checkbox-modal">
                        <label class="form-check-label" for="maintenance-checkbox-modal">
                            Véhicule en maintenance
                        </label>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary modification-btn-modal">Modifier</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
            </div>
        </div>
    </div>
</div>