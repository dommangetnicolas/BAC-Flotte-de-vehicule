<div class="container">
    <div class="row">
        <div class="col-12 col-lg-6">
            <div class="rounded-div">
                <h6>Trajets récents</h6><br>
                <ul class="list-group" id="recents-trips-home">
                    
                </ul><br>
                <a href="<?= URL ?>trips" class="btn btn-outline-success">Voir</a>
            </div>

            <div class="rounded-div">
                <h6>Ajouter un nouveau scooter</h6><br>
                <a href="<?= URL ?>configurevehicule" class="btn btn-outline-primary">Configurer en ligne </a>&nbsp
            </div>
        </div>
        <div class="col-12 col-lg-6">
            <div class="rounded-div">
                <h6>État du parc</h6><br>
                <div class="col-12 row" id="parc-home">
                    
                </div><br>
                <div class="col-12">
                    <div class="row">
                        <h6><span class="badge badge-success">Disponible</span></h6>&nbsp
                        <h6><span class="badge badge-warning">En maintenance</span></h6>&nbsp
                        <h6><span class="badge badge-danger">Indisponible</span></h6>
                    </div>
                </div>
                <a href="<?= URL ?>parc" class="btn btn-outline-success">Voir</a>
            </div>

            <div class="rounded-div">
                <h6>Maintenances à prévoir</h6><br>
                <ul class="list-group" id="maintenancetoprovide-home">

                </ul><br>
                <a href="<?= URL ?>maintenance" class="btn btn-outline-success">Voir</a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="add-scooter-modal-home" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Ajouter un scooter</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Pour configurer un nouveau scooter, il faut télécharger le logiciel, celui-ci configurera la carte et vous expliquera étape par étape comment configurer le scooter.</p>
      </div>
      <div class="modal-footer">
        <a href="#" class="btn btn-outline-primary">Télécharger <i class="fab fa-windows"></i></a>
        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>