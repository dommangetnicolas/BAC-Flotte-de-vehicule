import urllib2
import database
import os

class RegisterTrip:
    idDriver = 0
    idScooter = 0
    distance = 0
    duration = 0
    averageSpeed = 0
    fileName = 'trips.sql'

    def checkConnection(self):
        try:
            urllib2.urlopen("http://192.168.43.22").close()
            
        except urllib2.URLError:
            return False

        else:
            return True

    def setValues(self, idDriver, idScooter, distance, duration, averageSpeed):
        self.idDriver = idDriver
        self.idScooter = idScooter
        self.distance = distance
        self.duration = duration
        self.averageSpeed = averageSpeed

    def addTrip(self):
        db = database.DB()

        if os.path.exists(self.fileName) and self.checkConnection() == True:
            self.uploadSqlFile()
            
        db.insert("INSERT INTO trips (iddriver, idscooter, distance, duration, averagespeed) VALUES ('%s', '%s', '%s', '%s', '%s')" % (self.idDriver, self.idScooter, self.distance, self.duration, self.averageSpeed))

    def createSqlFile(self):
        db = database.DB(False)
        db.createSqlFile(self.idDriver, self.idScooter, self.distance, self.duration, self.averageSpeed, self.fileName)

    def uploadSqlFile(self):
        if os.path.exists(self.fileName) and self.checkConnection() == True:
                db = database.DB()
                db.insertSqlFile(self.fileName)
                os.remove(self.fileName)