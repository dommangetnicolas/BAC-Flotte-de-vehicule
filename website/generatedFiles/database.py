import MySQLdb

class DB():

    host = '192.168.1.149'
    user = 'dmg'
    password = '2269'
    db = 'fleet'
    useDb = True

    def __init__(self, useDb = True):
        self.useDb = useDb

        if(self.useDb == True):
            self.connection = MySQLdb.connect(self.host, self.user, self.password, self.db)
            self.cursor = self.connection.cursor()

    def insert(self, query):
        try:
            self.cursor.execute(query)
            self.connection.commit()
        except:
            self.connection.rollback()

    def insertSqlFile(self, fileName):
        for line in open(fileName):
            self.cursor.execute(line)
            self.connection.commit()

    def createSqlFile(self, idDriver, idScooter, distance, duration, averageSpeed, fileName):
        file = open(fileName, "a")
        file.write("INSERT INTO trips (iddriver, idscooter, distance, duration, averagespeed) VALUES (" + str(idDriver) + ", " + str(idScooter) + ", " + str(distance) + ", " + str(duration) + ", " + str(averageSpeed) + ")\n")
        file.close()

    def query(self, query):
        cursor = self.connection.cursor(MySQLdb.cursors.DictCursor)
        cursor.execute(query)

        return cursor.fetchall()

    def __del__(self):
        if(self.useDb == True):
            self.connection.close()