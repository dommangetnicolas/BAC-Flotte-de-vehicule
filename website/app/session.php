<?php
class session
{
	public static function init($id)
	{
		$_SESSION['id'] = $id;
	}
	public static function start()
	{
		if(!isset($_SESSION)) {
			session_start();
		}
	}
	public static function stop()
	{
		session_destroy();
		header('Location: ' . URL . 'login');
	}
	public static function connexionCheck($mode)
	{
		switch ($mode) {
			case 'disconnected': // Check if user is disconnected
				if(!isset($_SESSION['id'])) {
					header('Location: ' . URL . 'login');
				}
				break;
			
			case 'connected': // Check if user is connected
				if(isset($_SESSION['id'])) {
					header('Location: ' . URL);
				}
				break;
			default:
				# code...
				break;
		}
	}
}