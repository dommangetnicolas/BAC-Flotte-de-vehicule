<?php
class controller {
	public function __construct($mode = 'disconnected') {
		session::start();
		session::connexionCheck($mode);
		$this->view = new view();
	}
	public function loadModel($name) // If the page has a model, then we include the model
	{
		$file = 'model/' . $name . 'Model.php';
		if (file_exists($file)) {
			require $file;
			$modelName = $name . 'Model';
			$this->model = new $modelName();
		}
	}
}