<?php
class db extends PDO
{
	public function __construct($dbType, $dbHost, $dbName, $dbUser, $dbPass)
	{
		parent::__construct($dbType.':host='.$dbHost.';dbname='.$dbName, $dbUser, $dbPass);
	}
}