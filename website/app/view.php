<?php
class view {
	private $_folder = 'index/';
	private $_file = 'index';
	private $_template = 'default';
	public $title = 'Flotte de véhicules';
	private $_navbar = 'default';
	private $_path;
	public function render($arg) {
		if(isset($arg['folder'])) {
			$this->_folder = $arg['folder'] . '/';
		}
		if(isset($arg['template'])) {
			$this->_template = $arg['template'];
		}
		if(isset($arg['navbar'])) {
			$this->_navbar = $arg['navbar'];
		}
		if(isset($arg['title'])) {
			$this->title = ucfirst($arg['title']) . ' - ' . $this->title;
		}
		if(isset($arg['css'])) {
			$this->css = $arg['css'];
		}
		if(isset($arg['js'])) {
			$this->js = $arg['js'];
		}
		$this->_path = 'view/' . $this->_folder . $this->_file . '.php';
		$this->_template($this->_template, $this->_navbar);
	}
	private function _template($template, $navbar) {
		switch ($template) {
			case 'default':
				require 'view/bootstrap/head.php';
				if($navbar != 'none') {
					require 'view/bootstrap/navbar.php';
				}
				require $this->_path;
				require 'view/bootstrap/foot.php';
				break;
			case 'none':
				require $this->_path;
				break;
			
			default:
				# code...
				break;
		}
	}
}