<?php
class app {
	private $_url;
	private $_controller;
	public function init() {
		$this->_getUrl();
		$this->_loadController();
		$this->_callControllerMethod();
	}
	private function _getUrl() { // Analyse the url 
		$url = isset($_GET['url']) ? $_GET['url'] : null;
		$url = rtrim($url, '/');
		$url = filter_var($url, FILTER_SANITIZE_URL);
        $this->_url = explode('/', $url);
        if(empty($this->_url[0])) {
        	$this->_url[0] = 'index';
        }
	}
	private function _loadController() { // Choose the controller by reading _url[0] and load the controller
		$file = 'controller/' . $this->_url[0] . '.php';
		if(file_exists($file)) {
			require $file;
			$this->_controller = new $this->_url[0]();
			$this->_controller->loadModel($this->_url[0]);
		} else {
			$this->_mistake();
		}
	}
	private function _callControllerMethod() { // Call controller method
		$count = count($this->_url);
		if ($count > 1) {
			if (!method_exists($this->_controller, $this->_url[1])) {
				header('Location: ' . URL . 'mistake');
			}
		}
		
		switch ($count) {
			case 7:
				$this->_controller->{$this->_url[1]}($this->_url[2], $this->_url[3], $this->_url[4], $this->_url[5], $this->_url[6]);
				break;
			
			case 6:
				$this->_controller->{$this->_url[1]}($this->_url[2], $this->_url[3], $this->_url[4], $this->_url[5]);
				break;
			
			case 5:
				$this->_controller->{$this->_url[1]}($this->_url[2], $this->_url[3], $this->_url[4]);
				break;
			
			case 4:
				$this->_controller->{$this->_url[1]}($this->_url[2], $this->_url[3]);
				break;
			
			case 3:
				$this->_controller->{$this->_url[1]}($this->_url[2]);
				break;
				
			case 2:
				$this->_controller->{$this->_url[1]}();
				break;
			
			default:
				$this->_controller->default();
				break;
		}
	}
	private function _mistake() { // If requested page doesn't exist, show error message.
		require 'controller/mistake.php';
		$this->_controller = new mistake();
	}
}