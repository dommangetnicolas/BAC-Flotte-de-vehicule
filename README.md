# Flotte de véhicules

Projet de BAC STI S.I.N. 2017/2018

Ce projet a pour but d'aider à la gestion d'une flotte de véhicules deux roues, les 4 fonctions principals sont:
  - Assurer un suivi de la maintenance (Nicolas D.M.)
  - Archiver les itinéraires (Maxime K.)
  - Gérer les autorisations d’utilisation des véhicules (Noah L.)
  - Former à l’éco conduite (Maxime M.)

## Problématique

Comment optimiser la gestion d’une flotte de véhicules deux roues ?

## Description de la partie "Assurer un suivi de la maintenance"

  - Acquérir les données d’un véhicule (Distance, temps de parcours…).
  - Regrouper ces données dans une base de donnée afin de les afficher sur un site web dans le but de permettre à l’utilisateur de gérer de manière plus efficace et plus rapide une flotte de véhicule.
  - Créer un site web et/ou une application dans lequel nous pouvons voir l’état d’un véhicule (Kilométrage, distance avant la prochaine révision…)


## Revue de projet N°1

https://gitlab.com/dommanget.nicolas/flotte-de-vehicule/tree/master/Revue%20de%20projet%20N%C2%B01

## Revue de projet N°2

https://gitlab.com/dommanget.nicolas/flotte-de-vehicule/tree/master/Revue%20de%20projet%20N%C2%B02

## Revue de projet N°3

https://gitlab.com/dommanget.nicolas/flotte-de-vehicule/tree/master/Revue%20de%20projet%20N%C2%B03

## Revue de projet finale

https://gitlab.com/dommanget.nicolas/flotte-de-vehicule/tree/master/Revue%20de%20projet%20finale
