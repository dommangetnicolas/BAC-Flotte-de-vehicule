#!/usr/bin/env python
import RPi.GPIO as GPIO
import time
import registertrip
import os

class Speedometer:
        register = registertrip.RegisterTrip()
        uploadSqlInterval = time.time()
        reedPin = 11
        idDriver = 4
        idScooter = 8
        circumference = 2.05 #26 Inch
        stoppingDelay = 5

        def __init__(self):
                GPIO.setmode(GPIO.BOARD)
                GPIO.setup(self.reedPin, GPIO.IN)
        
        def loop(self):
                while True:
                    loop = 0
                    start = 0
                    duration = 0
                    distance = 0
                    stopping = 0
                    count = 0
                    stopped = False

                    if GPIO.input(self.reedPin) == 0:
                        start = time.time()
                        while loop == 0:
                            if GPIO.input(self.reedPin) == 0:
                                while(GPIO.input(self.reedPin) == 0):
                                    if(count == 0):
                                        stopping = 0
                                        stopped = False
                                        distance +=  self.circumference
                                        print(str(distance) + " Metres")
                                        count += 1
                                        time.sleep(0.05)
                                    else:
                                        pass
                            elif stopped == False and GPIO.input(self.reedPin) == 1:
                                count = 0
                                stopping = time.time()
                                stopped = True
                                time.sleep(0.001)
                            elif stopped == True and GPIO.input(self.reedPin) == 1 and time.time() - stopping < self.stoppingDelay:
                                count = 0
                                time.sleep(0.001)
                            else:
                                count = 0
                                distance /= 1000
                                duration = (time.time() - start - self.stoppingDelay / 3600) / 3600
                                self.endOfTrip(distance, duration, self.computeAverageSpeed(distance, duration))
                                loop = 1

                    if(time.time() - self.uploadSqlInterval > 30):
                        self.uploadSqlInterval = time.time()
                        self.register.uploadSqlFile()

                    time.sleep(0.001)

        def computeAverageSpeed(self, distance, duration):
            averageSpeed = (distance / duration)
            return averageSpeed

        def endOfTrip(self, distance, duration, averageSpeed):
            print("Distance parcourue: " + str(distance))
            print("Temps de parcours: " + str(duration))
            print("Vitesse moyenne: " + str(averageSpeed))
            if self.register.checkConnection() == True:
                self.register.setValues(self.idDriver, self.idScooter, distance, duration, averageSpeed)
                self.register.addTrip()
                self.register.uploadSqlFile()
            else:
                self.register.setValues(self.idDriver, self.idScooter, distance, duration, averageSpeed)
                self.register.createSqlFile()