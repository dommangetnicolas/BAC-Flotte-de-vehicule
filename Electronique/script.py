import speedometer
import RPi.GPIO as GPIO

def endprogram():
        GPIO.cleanup()

if __name__ == '__main__':
        speedometer = speedometer.Speedometer()
        try:
                speedometer.loop()
        except KeyboardInterrupt:
                endprogram()